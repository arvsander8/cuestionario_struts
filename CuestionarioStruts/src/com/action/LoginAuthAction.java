package com.action;

import com.opensymphony.xwork2.ActionSupport;

import com.funciones.*;

public class LoginAuthAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private String userId;
	private String password;

	private Boolean error;

	public String execute() {
		
		Integer rpt = new Funciones().verificarUsuario(userId,password);
		
		if (userId.equals("")) {
			addActionError("Ingrese su usuario.");
			error = true;
		} else if (password.equals("")) {
			addActionError("Ingrese su contraseņa.");
			error = true;
		} else if (rpt!=1) {
			addActionError("Passwordo usuario no valido.");
			error = true;
		} else {
			error = false;
		}

		if (error) {
			return ERROR;
		} else {
			return SUCCESS;
		}

	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}