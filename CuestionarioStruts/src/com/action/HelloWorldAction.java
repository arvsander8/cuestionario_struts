package com.action;
import com.opensymphony.xwork2.ActionSupport;

public class HelloWorldAction extends ActionSupport {

	private String message;

	public String execute() throws Exception{
		setMessage("Hello World Application");
		return SUCCESS;
	}

	public void setMessage(String message){
		this.message=message;
	}

	public String getMessage(){
		return message;
	}

}